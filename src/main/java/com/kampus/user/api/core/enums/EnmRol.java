package com.kampus.user.api.core.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
public enum EnmRol {
SUPER_ADMIN(1,"SUPER_ADMIN","SÜPER ADMİN"),
OGRENCI(3,"OGRENCI","ÖĞRENCİ"),
VELI(4,"VELI","VELİ"),
OGRETMEN(5,"OGRETMEN","ÖĞRETMEN"),
OKUL_YONETICISI(6,"OKUL_YONETICISI","OKUL YÖNETİCİSİ");

    private Integer id;
    private String kod;
    private String ad;

    public static EnmRol getByIntCode(int id) {
        for (EnmRol codes : EnmRol.values()) {
            if (id == codes.getId()) {
                return codes;
            }
        }
        return null;
    }

}
