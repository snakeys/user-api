package com.kampus.user.api.dto;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class OgrenciOwnUpdateDto extends RequestDto {
    private String cep;
    private String adres;
    private String email;
    private String oldPassword;
    private String newPassword;
    private String confirmNewPassword;
}
