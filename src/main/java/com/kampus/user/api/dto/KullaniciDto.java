package com.kampus.user.api.dto;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class KullaniciDto extends RequestDto {
    private String ad;
    private String soyad;
    private String cep;
    private String adres;
    private Long tc;
    private String email;
    private LocalDate dogumTarihi;
    private Integer cinsiyetId;
    private String veliId;
    private String okulId;
    private String bransId;
    private Integer sinifDerecesi;
    private String  okulSinifId;
    private String okulNo;
    private List<String> rolKods;
    private boolean silindiMi = false;
}
