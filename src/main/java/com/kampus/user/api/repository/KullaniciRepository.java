package com.kampus.user.api.repository;

import com.kampus.user.api.core.BaseRepository;
import com.kampus.user.api.entity.Kullanici;
import com.kampus.user.api.repository.custom.KullaniciCustomRepo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KullaniciRepository extends BaseRepository<Kullanici>, KullaniciCustomRepo {
    Optional<Kullanici> findByTc(Long tc);
    Optional<Kullanici> findByEmail(String email);
    Optional<Kullanici> findByOkulNoAndOkulId(String okulNo,String okulId);
    List<Kullanici> findAllByOkulSinifId(String okulSinifId);
}
