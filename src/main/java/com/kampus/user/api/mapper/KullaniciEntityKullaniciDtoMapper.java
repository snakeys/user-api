package com.kampus.user.api.mapper;

import com.kampus.user.api.dto.KullaniciDto;
import com.kampus.user.api.dto.OgrenciDto;
import com.kampus.user.api.entity.Kullanici;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KullaniciEntityKullaniciDtoMapper {

    @Mapping(target = "roller", ignore = true)
    @Mapping(target = "sifre", ignore = true)
    Kullanici dtoToEntity(KullaniciDto dto);

    @Mapping(target = "rolKods", ignore = true)
    KullaniciDto entityToDto(Kullanici kullanici);

    List<KullaniciDto> entitiesToDtos(List<Kullanici> kullanicis);
}
