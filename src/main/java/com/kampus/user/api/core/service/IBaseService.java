package com.kampus.user.api.core.service;

import com.kampus.user.api.core.dto.Dto;

import java.util.List;

public interface IBaseService<T extends Dto,ID> {
    T save (T dto);

    T updateById(T dto, ID id);

    boolean deleteById(ID id);

    T findById(ID id);
}
