package com.kampus.user.api.core.dto;

import com.kampus.user.api.core.enums.EnmCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseDto extends BaseDto {

    private EnmCode enmCode;
    private int code;
    private String message;
    private Object data;

    public ResponseDto(EnmCode codeType, String message, Object data) {
        code = codeType.getCode();
        this.message = message;
        this.data = data;
    }

    public static ResponseDto done(String message) {
        return new ResponseDto(EnmCode.SUCCESS, message, null);
    }

    public static ResponseDto done(Object data) {
        return new ResponseDto(EnmCode.SUCCESS, null, data);
    }

    public static ResponseDto done(String message, Object data) {
        return new ResponseDto(EnmCode.SUCCESS, message, data);
    }

    public static ResponseDto error(EnmCode code) {

        return new ResponseDto(code, null, null);
    }

    public static ResponseDto error(EnmCode code, String message) {

        return new ResponseDto(code, message, null);
    }

    public static ResponseDto error(EnmCode code, Object data) {

        return new ResponseDto(code, null, data);
    }

    public static ResponseDto error(EnmCode code, String message, Object data) {
        return new ResponseDto(code, message, data);
    }

    public EnmCode getEnmCode() {
        return EnmCode.getByIntCode(this.code);
    }

}
