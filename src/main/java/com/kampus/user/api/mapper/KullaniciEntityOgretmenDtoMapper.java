package com.kampus.user.api.mapper;

import com.kampus.user.api.dto.OgretmenDto;
import com.kampus.user.api.entity.Kullanici;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KullaniciEntityOgretmenDtoMapper {

    List<OgretmenDto> entitiesToDtos(List<Kullanici> kullanicis);

}
