package com.kampus.user.api.core.exception;

import com.kampus.user.api.core.enums.EnmCode;

public class NotDeletedException extends BaseCustomException {
    public NotDeletedException(String errorMessage) {
        super(EnmCode.NOT_DELETED, errorMessage);
    }
}
