package com.kampus.user.api.repository.custom;

import com.kampus.user.api.core.helper.StringHelper;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.entity.Kullanici;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class KullaniciCustomRepoImpl implements KullaniciCustomRepo {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Kullanici> findKullanicisByProperties(KullaniciFilterDto kullaniciFilterDto, Pageable pageable) {
        final Query query = new Query().with(pageable);
        final List<Criteria> criteria = new ArrayList<>();


        if (!StringHelper.isNullOrEmpty(kullaniciFilterDto.getAd())) {
            criteria.add(Criteria.where("ad").regex(StringHelper.toUpper(kullaniciFilterDto.getAd())));
        }
        if (!StringHelper.isNullOrEmpty(kullaniciFilterDto.getSoyad())) {
            criteria.add(Criteria.where("soyad").regex(StringHelper.toUpper(kullaniciFilterDto.getSoyad())));
        }
        if (!StringHelper.isNullOrEmpty(kullaniciFilterDto.getTc())) {
            long tc = Long.parseLong(kullaniciFilterDto.getTc());
            criteria.add(Criteria.where("tc").is(tc));
        }
        if (!StringHelper.isNullOrEmpty(kullaniciFilterDto.getOkulNo())) {
            criteria.add(Criteria.where("okulNo").is(kullaniciFilterDto.getOkulNo()));
        }
        if (!StringHelper.isNullOrEmpty(kullaniciFilterDto.getSinifDerecesi())) {
            int sinifDerecesi = Integer.parseInt(kullaniciFilterDto.getSinifDerecesi());
            criteria.add(Criteria.where("sinifDerecesi").is(sinifDerecesi));
        }
        criteria.add(Criteria.where("silindiMi").is(false));
        criteria.add(Criteria.where("okulId").is(kullaniciFilterDto.getOkulId()));
        /*
        if (!StringHelper.isNullOrEmpty(ogrenciFilterDto.getSube())){
            criteria.add(Criteria.where("sube").is(ogrenciFilterDto.getSube()));
        }*/
        if (!criteria.isEmpty()) {
            query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
        }
        List<Kullanici> kullaniciList = mongoTemplate.find(query, Kullanici.class);
        return kullaniciList;
    }
}
