package com.kampus.user.api.core.exception;

import com.kampus.user.api.core.enums.EnmCode;

public class NotFoundException extends BaseCustomException {
    public NotFoundException(String errorMessage) {
        super(EnmCode.NOT_FOUND,errorMessage);
    }
}
