package com.kampus.user.api.repository;

import com.kampus.user.api.core.BaseRepository;
import com.kampus.user.api.entity.KullaniciRol;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KullaniciRolRepository extends BaseRepository<KullaniciRol> {

    Optional<List<KullaniciRol>> findByRolIdIn(String rolId);

    Optional<List<KullaniciRol>> findByKullaniciIdIn(String kullaniciId);
}
