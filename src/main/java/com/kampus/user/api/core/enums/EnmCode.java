package com.kampus.user.api.core.enums;

public enum EnmCode {
    SUCCESS(10, "success"),
    EXIST(11, "error.exists"),
    INVALID(12, "error.invalid"),
    NOT_FOUND(13, "error.not.found"),
    NOT_UPDATED(14, "error.not.updated"),
    NOT_PERMITTED(15, "error.not.permitted"),
    AUTHENTICATION(16, "error.authentication"),
    NOT_DELETED(17, "error.not.permitted"),
    MISSING(18, "error.missing"),
    UNKNOWN(99, "error.unknown"),
    DB(100, "error.db");

    private int code;
    private String message;

    EnmCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static EnmCode getByIntCode(int code) {
        for (EnmCode codes : EnmCode.values()) {
            if (code == codes.getCode()) {
                return codes;
            }
        }
        return null;
    }

    public static EnmCode getByMessage(String message) {
        for (EnmCode codes : EnmCode.values()) {
            if (message.equals(codes.getMessage())) {
                return codes;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
