package com.kampus.user.api.service;

import com.kampus.user.api.core.dto.Dto;
import com.kampus.user.api.core.service.IBaseService;
import com.kampus.user.api.core.util.TPage;
import com.kampus.user.api.dto.KullaniciDto;
import com.kampus.user.api.dto.ManagerDto;
import com.kampus.user.api.dto.OgretmenDto;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.entity.Kullanici;
import org.springframework.data.domain.Pageable;

public interface IKullaniciService<E extends Dto, I extends String> extends IBaseService<KullaniciDto, String> {

    Kullanici getKullaniciByUsername(String username);

    TPage<ManagerDto> findByPagingCriteriaManager(KullaniciFilterDto kullaniciFilterDto,
                                                  Pageable pageable);

    TPage<OgretmenDto> findByPagingCriteriaOgretmen(KullaniciFilterDto kullaniciFilterDto,
                                                    Pageable pageable);
}
