package com.kampus.user.api.entity;

import com.kampus.user.api.core.BaseEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;
import java.util.HashSet;


@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "kullanici")
public class Kullanici extends BaseEntity {
    @Field("ad")
    private String ad;
    @Field("soyad")
    private String soyad;
    @Field("cep")
    private String cep;
    @Field("adres")
    private String adres;
    @Field("tc")
    private Long tc;
    @Field("email")
    private String email;
    @Field("dogum_tarihi")
    private LocalDate dogumTarihi;
    @Field("sifre")
    private String sifre;
    @Field("cinsiyet_id")
    private Integer cinsiyetId;
    @Field("veli_id")
    private String veliId;
    @Field("okul_id")
    private String okulId;
    @Field("brans_id")
    private String bransId;
    @Field("sinif_derecesi")
    private Integer sinifDerecesi;
    @Field("okul_sinif_id")
    private String okulSinifId;
    @Field("okul_no")
    private String okulNo;
    @Field("silindi_mi")
    private boolean silindiMi;
    @DBRef
    private HashSet<Rol> roller;
}
