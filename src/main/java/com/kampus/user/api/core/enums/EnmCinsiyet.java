package com.kampus.user.api.core.enums;

import lombok.Getter;

@Getter
public enum EnmCinsiyet {
    ERKEK(1,"ERKEK"),
    KADIN(2,"KADIN");


    private int Id;
    private String name;

    EnmCinsiyet(int id,String name){
        this.Id = id;
        this.name = name;
    };
}
