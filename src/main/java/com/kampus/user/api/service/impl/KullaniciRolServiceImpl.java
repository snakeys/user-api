package com.kampus.user.api.service.impl;

import com.kampus.user.api.core.dto.RequestDto;
import com.kampus.user.api.core.exception.NotDeletedException;
import com.kampus.user.api.core.service.BaseService;
import com.kampus.user.api.dto.KullaniciRolDto;
import com.kampus.user.api.entity.KullaniciRol;
import com.kampus.user.api.repository.KullaniciRolRepository;
import com.kampus.user.api.service.IKullaniciRolService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KullaniciRolServiceImpl extends BaseService implements IKullaniciRolService<RequestDto, String> {

    @Autowired
    KullaniciRolRepository kullaniciRolRepository;

    @Override
    public KullaniciRolDto save(KullaniciRolDto dto) {
        return null;
    }

    @Override
    public KullaniciRolDto updateById(KullaniciRolDto dto, String s) {
        return null;
    }

    @Override
    public boolean deleteById(String kullaniciId) {
        Optional<List<KullaniciRol>> kullaniciRolList = kullaniciRolRepository.findByKullaniciIdIn(kullaniciId);
        if (kullaniciRolList.get().size() > 0) {
            kullaniciRolList.get().stream().map(kullaniciRol -> {
                kullaniciRol.setSilindiMi(true);
                kullaniciRolRepository.save(kullaniciRol);
                return true;
            });
        }
        throw new NotDeletedException("error");
    }

    @Override
    public KullaniciRolDto findById(String s) {
        return null;
    }

    @Override
    public List<String> getRoleIdsByUserId(String userId) {
        Optional<List<KullaniciRol>> kullaniciRols = kullaniciRolRepository.findByKullaniciIdIn(userId);
        List<String> userRoleIds = new ArrayList<>();
        if (kullaniciRols.get().size() > 0) {
            kullaniciRols.get().forEach(kullaniciRol -> {
                userRoleIds.add(kullaniciRol.getRolId());
            });
        }
        return userRoleIds;
    }
}
