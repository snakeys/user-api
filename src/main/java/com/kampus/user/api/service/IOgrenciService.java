package com.kampus.user.api.service;

import com.kampus.user.api.core.dto.Dto;
import com.kampus.user.api.core.service.IBaseService;
import com.kampus.user.api.core.util.TPage;
import com.kampus.user.api.dto.OgrenciDto;
import com.kampus.user.api.dto.OgrenciOwnUpdateDto;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IOgrenciService<E extends Dto, I extends String> extends IBaseService<OgrenciDto, String> {

    TPage<OgrenciDto> findByPagingCriteria(KullaniciFilterDto kullaniciFilterDto,
                                           Pageable pageable);

    List<OgrenciDto> getAllOgrenciByOkulSinifId(String okulSinifId);

    OgrenciDto updateOwnInfobyOgrenci(String id, OgrenciOwnUpdateDto ogrenciDto);
}
