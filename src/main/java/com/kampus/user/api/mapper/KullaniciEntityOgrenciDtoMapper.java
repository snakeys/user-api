package com.kampus.user.api.mapper;

import com.kampus.user.api.dto.OgrenciDto;
import com.kampus.user.api.entity.Kullanici;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KullaniciEntityOgrenciDtoMapper {

    @Mapping(target = "roller", ignore = true)
    @Mapping(target = "sifre", ignore = true)
    @Mapping(target = "bransId", ignore = true)
    Kullanici dtoToEntity(OgrenciDto dto);

    OgrenciDto entityToDto(Kullanici kullanici);

    List<OgrenciDto> entitiesToDtos(List<Kullanici> views);
}
