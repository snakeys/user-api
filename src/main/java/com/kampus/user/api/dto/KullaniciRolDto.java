package com.kampus.user.api.dto;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class KullaniciRolDto extends RequestDto {
    private String kullaniciId;
    private String rolId;
    private boolean silindiMi = false;
}
