package com.kampus.user.api.dto.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiResponseDto {
    private String enmCode;
    private int code;
    private String message;
    private Object data;
}
