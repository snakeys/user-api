package com.kampus.user.api.repository.custom;

import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.entity.Kullanici;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface KullaniciCustomRepo {
    public List<Kullanici> findKullanicisByProperties(KullaniciFilterDto kullaniciFilterDto, Pageable page);
}
