package com.kampus.user.api.core.exception;

import com.kampus.user.api.core.enums.EnmCode;

public class InvalidException extends BaseCustomException {
    public InvalidException(String errorMessage) {
        super(EnmCode.INVALID, errorMessage);
    }
}
