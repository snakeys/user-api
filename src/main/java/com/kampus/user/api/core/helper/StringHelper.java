package com.kampus.user.api.core.helper;

import java.util.Locale;

public class StringHelper {

    public static final String ERRORPRE = "********* ERROR ";

    public static boolean isNullOrEmpty(final String str) {
        return isNullOrEmpty(str, true);
    }

    public static boolean isNullOrEmpty(final String str, final boolean trim) {
        return str == null
                || (trim ? str.trim().length() == 0 : str.length() == 0);
    }

    public static boolean equalIgnoreCase(String p1, String p2) {

        return p1.equalsIgnoreCase(p2);
    }

    public static String toUpper(String string) {
        return string.toUpperCase(new Locale("tr"));
    }

}
