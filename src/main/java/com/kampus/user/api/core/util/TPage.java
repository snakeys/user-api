package com.kampus.user.api.core.util;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class TPage<T> implements Serializable {
    private List<T> result;
    private TPagination pagination;

    public void setStat(Page page, List<T> list) {
        this.pagination = new
                TPagination(page.getNumber(),
                page.getSize(),
                page.getSort(),
                page.getTotalPages(),
                page.getTotalElements());
        this.result = list;
    }
}