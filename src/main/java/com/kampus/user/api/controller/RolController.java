package com.kampus.user.api.controller;

import com.kampus.user.api.core.controller.GenericRestController;
import com.kampus.user.api.core.dto.ResponseDto;
import com.kampus.user.api.dto.KullaniciDto;
import com.kampus.user.api.dto.RolDto;
import com.kampus.user.api.service.IKullaniciService;
import com.kampus.user.api.service.IRolService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rol")
@AllArgsConstructor
public class RolController extends GenericRestController<RolDto, String, IRolService<RolDto, String>> {

    @Autowired
    IRolService rolService;

    @Override
    public ResponseDto create(@RequestBody RolDto dto) {
        final RolDto savedDto = (RolDto) rolService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"),savedDto);
    }

    @Override
    public ResponseDto update(@PathVariable(value = "id") String id, @RequestBody RolDto dto) {
        final  RolDto updatedDto = (RolDto) rolService.updateById(dto,id);
        return ResponseDto.done(getMessageFromSource("success"),updatedDto);
    }

    @Override
    public ResponseDto getById(@PathVariable(value = "id") String id) {
        final RolDto rolDto = (RolDto) rolService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"),rolDto);
    }

    @Override
    public ResponseDto deleteById(@PathVariable(value = "id") String id) {
        final boolean isDeleted = rolService.deleteById(id);
        return ResponseDto.done(getMessageFromSource("success"),isDeleted);
    }
}
