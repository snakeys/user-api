package com.kampus.user.api.entity;

import com.kampus.user.api.core.BaseEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "kullanici_rol")
public class KullaniciRol extends BaseEntity {
    @Field("kullanici_id")
    private String kullaniciId;
    @Field("rol_id")
    private String rolId;
    @Field("silindi_mi")
    private boolean silindiMi;

}
