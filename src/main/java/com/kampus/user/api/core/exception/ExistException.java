package com.kampus.user.api.core.exception;

import com.kampus.user.api.core.enums.EnmCode;

public class ExistException extends BaseCustomException {
    public ExistException(String errorMessage) {
        super(EnmCode.EXIST, errorMessage);
    }
}
