package com.kampus.user.api.core.constants;

public class ApiUrlConstants {

    public static final String MANAGER_API_BASE_URL = "http://localhost:9002/api/";
    public static final String M_OKUL_API_URL = MANAGER_API_BASE_URL + "okul/";
    public static final String M_BRANS_API_URL = MANAGER_API_BASE_URL + "brans/";
}
