package com.kampus.user.api.core.controller;

import com.kampus.user.api.core.dto.Dto;
import com.kampus.user.api.core.dto.ResponseDto;
import com.kampus.user.api.core.service.IBaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
public class GenericRestController<T extends Dto, ID, S extends IBaseService<T, ID>> extends BaseAbstractController {


    private IBaseService<T, ID> crudService;


    @GetMapping("/{id}")
    public ResponseDto getById(@PathVariable(value = "id") ID id) {
        T dto = crudService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"), dto);
    }

    @PostMapping()
    public ResponseDto create(@RequestBody T dto) {
        crudService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"), dto);
    }

    @PutMapping("/{id}")
    public ResponseDto update(@PathVariable(value = "id") ID id,
                              @RequestBody T dto) {
        T t = crudService.updateById(dto, id);
        return ResponseDto.done(getMessageFromSource("success"), t);
    }

    @DeleteMapping("/{id}")
    public ResponseDto deleteById(@PathVariable(value = "id") ID id) {
        boolean isSuccess = crudService.deleteById(id);
        return ResponseDto.done("success", isSuccess);
    }


}