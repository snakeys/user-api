package com.kampus.user.api.dto.filter;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class KullaniciFilterDto extends RequestDto {
    private String ad;
    private String soyad;
    private String tc;
    private String okulNo;
    private String telNo;
    private String brans;
    private String sinifDerecesi;
    private String sube;
    private String okulId;
}
