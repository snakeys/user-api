package com.kampus.user.api.repository;

import com.kampus.user.api.core.BaseRepository;
import com.kampus.user.api.entity.Rol;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RolRepository extends BaseRepository<Rol> {
    Optional<Rol> findByKod(String kod);
    Optional<Set<Rol>> findByKodIn(List<String> kods);
}
