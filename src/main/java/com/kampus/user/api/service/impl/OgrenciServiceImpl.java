package com.kampus.user.api.service.impl;

import com.kampus.user.api.core.dto.RequestDto;
import com.kampus.user.api.core.enums.EnmRol;
import com.kampus.user.api.core.exception.ExistException;
import com.kampus.user.api.core.exception.InvalidException;
import com.kampus.user.api.core.exception.NotFoundException;
import com.kampus.user.api.core.helper.StringHelper;
import com.kampus.user.api.core.service.BaseService;
import com.kampus.user.api.core.util.TPage;
import com.kampus.user.api.dto.OgrenciDto;
import com.kampus.user.api.dto.OgrenciOwnUpdateDto;
import com.kampus.user.api.dto.api.ApiResponseDto;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.entity.Kullanici;
import com.kampus.user.api.entity.KullaniciRol;
import com.kampus.user.api.entity.Rol;
import com.kampus.user.api.mapper.KullaniciEntityOgrenciDtoMapper;
import com.kampus.user.api.repository.KullaniciRepository;
import com.kampus.user.api.repository.KullaniciRolRepository;
import com.kampus.user.api.repository.RolRepository;
import com.kampus.user.api.service.IOgrenciService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.kampus.user.api.core.constants.ApiUrlConstants.M_OKUL_API_URL;

@Service
@RequiredArgsConstructor
public class OgrenciServiceImpl extends BaseService implements IOgrenciService<RequestDto, String> {

    @Autowired
    KullaniciRepository kullaniciRepository;
    @Autowired
    KullaniciEntityOgrenciDtoMapper kullaniciEntityOgrenciDtoMapper;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    RolRepository rolRepository;
    @Autowired
    KullaniciRolRepository kullaniciRolRepository;
    @Autowired
    RestTemplate restTemplate;


    @Override
    public OgrenciDto save(OgrenciDto dto) {
        Optional<Kullanici> existOgrenci = kullaniciRepository.findByTc(dto.getTc());
        if (existOgrenci.isPresent()) {
            throw new ExistException("error");
        }

        if (dto.getOkulNo().isEmpty()) {
            throw new InvalidException("error");
        }

        Optional<Kullanici> existOkulNo = kullaniciRepository.findByOkulNoAndOkulId(dto.getOkulNo(), dto.getOkulId());
        if (existOkulNo.isPresent()) {
            throw new ExistException("error");
        }

        if (dto.getSinifDerecesi() < 1 && dto.getSinifDerecesi() > 12) {
            throw new InvalidException("error");
        }


        if (dto.getOkulId().isEmpty()) {
            throw new InvalidException("error");
        }

        ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(M_OKUL_API_URL + dto.getOkulId(), ApiResponseDto.class);
        if (responseDto.getBody() == null && responseDto.getStatusCode() != HttpStatus.OK) {
            throw new NotFoundException("error");
        }

        if (!dto.getVeliId().isEmpty()) {
            Optional<Kullanici> existVeli = kullaniciRepository.findById(dto.getVeliId());
            if (!existVeli.isPresent()) {
                throw new NotFoundException("error");
            }
        }


        dto.setAd(StringHelper.toUpper(dto.getAd()));
        dto.setSoyad(StringHelper.toUpper(dto.getSoyad()));
        dto.setCep(dto.getCep().replaceAll("\\s+", ""));
        Kullanici newOgrenci = kullaniciEntityOgrenciDtoMapper.dtoToEntity(dto);
        String password = firstPassword(dto.getTc());
        Rol rol = getRoles(EnmRol.OGRENCI.getKod());
        HashSet<Rol> rolSet = new HashSet<Rol>();
        rolSet.add(rol);

        newOgrenci.setSifre(bCryptPasswordEncoder.encode(password));
        newOgrenci.setRoller(rolSet);
        kullaniciRepository.save(newOgrenci);
        setKullaniciRol(rol, newOgrenci);
        dto.setId(newOgrenci.getId());
        return dto;

    }

    @Override
    public OgrenciDto updateById(OgrenciDto dto, String id) {
        Optional<Kullanici> existOgrenci = kullaniciRepository.findById(id);
        if (!existOgrenci.isPresent()) {
            throw new NotFoundException("error");
        }

        if (!existOgrenci.get().getOkulId().equals(dto.getOkulId())) {
            Optional<Kullanici> existOkulNo = kullaniciRepository.findByOkulNoAndOkulId(dto.getOkulNo(), dto.getOkulId());
            if (existOgrenci.isPresent()) {
                throw new ExistException("error");
            }
        }

        if (!dto.getVeliId().isEmpty() && !existOgrenci.get().getVeliId().equals(dto.getVeliId())) {
            Optional<Kullanici> existVeli = kullaniciRepository.findById(dto.getVeliId());
            if (!existVeli.isPresent()) {
                throw new NotFoundException("error");
            }
        }
        if (dto.getSinifDerecesi() < 1 && dto.getSinifDerecesi() > 12) {
            throw new InvalidException("error");
        }
        if (existOgrenci.get().getTc() != dto.getTc()) {
            Optional<Kullanici> existOgrenciTc = kullaniciRepository.findByTc(dto.getTc());
            if (existOgrenciTc.isPresent()) {
                throw new ExistException("error");
            }
        }
        if (!dto.getOkulId().isEmpty()) {
            ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(M_OKUL_API_URL + dto.getOkulId(), ApiResponseDto.class);
            if (responseDto.getBody() == null && responseDto.getStatusCode() != HttpStatus.OK) {
                throw new NotFoundException("error");
            }
        }

        if (!dto.getAd().isEmpty()) {
            dto.setAd(StringHelper.toUpper(dto.getAd()));
        }
        if (!dto.getSoyad().isEmpty()) {
            dto.setSoyad(StringHelper.toUpper(dto.getSoyad()));
        }
        if (!dto.getCep().isEmpty()) {
            dto.setCep(dto.getCep().replaceAll("\\s+", ""));
        }
        Kullanici updatedKullanici = kullaniciEntityOgrenciDtoMapper.dtoToEntity(dto);
        kullaniciRepository.save(updatedKullanici);
        dto.setId(updatedKullanici.getId());
        return kullaniciEntityOgrenciDtoMapper.entityToDto(updatedKullanici);

    }

    @Override
    public boolean deleteById(String id) {
        Optional<Kullanici> existOgrenci = kullaniciRepository.findById(id);
        if (!existOgrenci.isPresent()) {
            throw new NotFoundException("error");
        }
        kullaniciRepository.deleteById(id);
        return true;
    }

    @Override
    public OgrenciDto findById(String id) {
        Optional<Kullanici> existOgrenci = kullaniciRepository.findById(id);
        if (!existOgrenci.isPresent()) {
            throw new NotFoundException("error");
        }
        return kullaniciEntityOgrenciDtoMapper.entityToDto(existOgrenci.get());
    }

    private String firstPassword(Long tc) {

        if (tc != null) {
            return tc.toString().substring(5, 11);
        }

        return "123456";
    }

    private Rol getRoles(String rolKod) {

        if (!rolKod.isEmpty()) {
            Optional<Rol> rol = rolRepository.findByKod(rolKod);
            if (rol.isPresent()) {
                return rol.get();
            }
        }
        return null;
    }

    private void setKullaniciRol(Rol rol, Kullanici newOgrenci) {
        KullaniciRol kullaniciRol = new KullaniciRol();
        kullaniciRol.setRolId(rol.getId());
        kullaniciRol.setKullaniciId(newOgrenci.getId());
        kullaniciRolRepository.save(kullaniciRol);

    }

    @Override
    public TPage<OgrenciDto> findByPagingCriteria(KullaniciFilterDto kullaniciFilterDto, Pageable pageable) {

        Optional<Rol> rol = rolRepository.findByKod(EnmRol.OGRENCI.getKod());
        List<Kullanici> kullaniciList = kullaniciRepository.findKullanicisByProperties(kullaniciFilterDto, pageable);
        List<Kullanici> ogrenciList = new ArrayList<>();
        kullaniciList.forEach(kullanici -> {
            kullanici.getRoller().forEach(
                    userRol -> {
                        if (userRol.getKod().equals(rol.get().getKod())) {
                            ogrenciList.add(kullanici);
                            return;
                        }
                    }
            );
        });
        List<OgrenciDto> data = kullaniciEntityOgrenciDtoMapper.entitiesToDtos(ogrenciList);
        TPage<OgrenciDto> response = new TPage<>();
        response.setResult(data);

        return response;
    }

    @Override
    public List<OgrenciDto> getAllOgrenciByOkulSinifId(String okulSinifId) {
        List<Kullanici> kullaniciList = kullaniciRepository.findAllByOkulSinifId(okulSinifId);
        if (kullaniciList.isEmpty()) {
            return null;
        }
        return kullaniciEntityOgrenciDtoMapper.entitiesToDtos(kullaniciList);
    }

    @Override
    public OgrenciDto updateOwnInfobyOgrenci(String id, OgrenciOwnUpdateDto ogrenciDto) {
        Optional<Kullanici> existOgrenci = kullaniciRepository.findById(id);
        if (!existOgrenci.isPresent()) {
            throw new NotFoundException("error");
        }
        Kullanici updatedOgrenci = existOgrenci.get();
        if (!updatedOgrenci.getCep().equals(ogrenciDto.getCep())) {
            updatedOgrenci.setCep(ogrenciDto.getCep().replaceAll("\\s+", ""));
        }
        if (!updatedOgrenci.getEmail().equals(ogrenciDto.getEmail())) {
            updatedOgrenci.setEmail(ogrenciDto.getEmail());
        }
        if (!updatedOgrenci.getAdres().equals(ogrenciDto.getAdres())) {
            updatedOgrenci.setAdres(ogrenciDto.getAdres());
        }
        if (!StringHelper.isNullOrEmpty(ogrenciDto.getOldPassword()) && !StringHelper.isNullOrEmpty(ogrenciDto.getNewPassword())
                && !StringHelper.isNullOrEmpty(ogrenciDto.getConfirmNewPassword())) {
            boolean passMatches = bCryptPasswordEncoder.matches(ogrenciDto.getOldPassword(), updatedOgrenci.getSifre());
            if (passMatches) {
                if (ogrenciDto.getNewPassword().equals(ogrenciDto.getConfirmNewPassword())) {
                    updatedOgrenci.setSifre(bCryptPasswordEncoder.encode(ogrenciDto.getNewPassword()));
                }
            }
            throw new InvalidException("error");
        }
        kullaniciRepository.save(updatedOgrenci);
        return kullaniciEntityOgrenciDtoMapper.entityToDto(updatedOgrenci);

    }
}
