package com.kampus.user.api.dto;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class OgrenciDto extends RequestDto {
    private String ad;
    private String soyad;
    private String cep;
    private String adres;
    private Long tc;
    private String email;
    private LocalDate dogumTarihi;
    private Integer cinsiyetId;
    private String veliId;
    private String okulId;
    private Integer sinifDerecesi;
    private String okulNo;
    private String okulSinifId;
    private boolean silindiMi = false;
}
