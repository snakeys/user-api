package com.kampus.user.api.entity;

import com.kampus.user.api.core.BaseEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection  = "rol")
public class Rol extends BaseEntity{
    @Field("ad")
    private String ad;
    @Field("kod")
    private String kod;
    @Field("silindi_mi")
    private boolean silindiMi;
}
