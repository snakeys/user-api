package com.kampus.user.api.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;



@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class BaseDto implements Dto{

}

