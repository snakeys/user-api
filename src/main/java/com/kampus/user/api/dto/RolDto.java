package com.kampus.user.api.dto;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RolDto extends RequestDto {
    private String ad;
    private String kod;
    private boolean silindiMi = false;
}
