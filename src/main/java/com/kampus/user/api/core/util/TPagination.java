package com.kampus.user.api.core.util;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class TPagination implements Serializable {
    private int number;
    private int size;
    private Sort sort;
    private int totalPages;
    private Long totalElements;

    public TPagination(int number, int size, Sort sort, int totalPages, Long totalElements) {
        this.number = number;
        this.size = size;
        this.sort = sort;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }
}