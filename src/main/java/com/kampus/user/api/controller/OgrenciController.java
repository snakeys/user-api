package com.kampus.user.api.controller;

import com.kampus.user.api.core.controller.GenericRestController;
import com.kampus.user.api.core.dto.ResponseDto;
import com.kampus.user.api.core.util.TPage;
import com.kampus.user.api.dto.OgrenciDto;
import com.kampus.user.api.dto.OgrenciOwnUpdateDto;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.service.IOgrenciService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ogrenci")
@AllArgsConstructor
public class OgrenciController extends GenericRestController<OgrenciDto, String, IOgrenciService<OgrenciDto, String>> {

    @Autowired
    IOgrenciService ogrenciService;

    @Override
    public ResponseDto create(@RequestBody OgrenciDto dto) {
        final OgrenciDto ogrenciDto = (OgrenciDto) ogrenciService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"), ogrenciDto);
    }

    @Override
    public ResponseDto update(@PathVariable(value = "id") String id, @RequestBody OgrenciDto dto) {
        final OgrenciDto ogrenciDto = (OgrenciDto) ogrenciService.updateById(dto, id);
        return ResponseDto.done(getMessageFromSource("success"), ogrenciDto);
    }

    @PutMapping("/info/{id}")
    public ResponseDto updateOwnInfoByOgrenci(@PathVariable(value = "id") String id, @RequestBody OgrenciOwnUpdateDto dto) {
        final OgrenciDto ogrenciDto = (OgrenciDto) ogrenciService.updateOwnInfobyOgrenci(id, dto);
        return ResponseDto.done(getMessageFromSource("success"), ogrenciDto);
    }

    @Override
    public ResponseDto getById(@PathVariable(value = "id") String id) {
        final OgrenciDto ogrenciDto = (OgrenciDto) ogrenciService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"), ogrenciDto);
    }

    @GetMapping("/pagination-filter")
    public ResponseDto getAllOgrenciByPagination(Pageable pageable,
                                                   KullaniciFilterDto kullaniciFilterDto) {

        TPage<OgrenciDto> data = ogrenciService.findByPagingCriteria(
                kullaniciFilterDto,
                pageable);

        return ResponseDto.done(getMessageFromSource("success"), data);

    }

    @GetMapping("/sinif/{okulSinifId}")
    public ResponseDto getAllOgrenciBySinifId(@PathVariable(value = "okulSinifId") String okulSinifId) {

        List<OgrenciDto> ogrenciDtos = ogrenciService.getAllOgrenciByOkulSinifId(okulSinifId);
        return ResponseDto.done(getMessageFromSource("success"), ogrenciDtos);

    }


}
