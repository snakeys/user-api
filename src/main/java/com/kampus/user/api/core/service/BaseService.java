package com.kampus.user.api.core.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseService {
    @Autowired
    MessageSource messageSource;

    String getMessageFromSource(String messageKey) {
        return messageSource.getMessage(messageKey, new Object[0], LocaleContextHolder.getLocale());
    }
}
