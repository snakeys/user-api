package com.kampus.user.api.service.impl;

import com.kampus.user.api.core.dto.RequestDto;
import com.kampus.user.api.core.exception.ExistException;
import com.kampus.user.api.core.exception.NotDeletedException;
import com.kampus.user.api.core.exception.NotFoundException;
import com.kampus.user.api.core.service.BaseService;
import com.kampus.user.api.dto.RolDto;
import com.kampus.user.api.entity.Rol;
import com.kampus.user.api.mapper.RolEntityRolDtoMapper;
import com.kampus.user.api.repository.KullaniciRolRepository;
import com.kampus.user.api.repository.RolRepository;
import com.kampus.user.api.service.IKullaniciRolService;
import com.kampus.user.api.service.IRolService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RolServiceImpl extends BaseService implements IRolService<RequestDto, String> {

    @Autowired
    RolRepository rolRepository;
    @Autowired
    RolEntityRolDtoMapper rolEntityRolDtoMapper;
    @Autowired
    KullaniciRolRepository kullaniciRolRepository;
    @Autowired
    IKullaniciRolService kullaniciRolService;


    @Override
    public RolDto save(RolDto dto) {
        Optional<Rol> rol = rolRepository.findByKod(dto.getKod());
        if (rol.isPresent()) {
            throw new ExistException("error");
        }
        Rol newRol = rolEntityRolDtoMapper.dtoToEntity(dto);
        rolRepository.save(newRol);
        dto.setId(newRol.getId());
        return dto;
    }

    @Override
    public RolDto updateById(RolDto dto, String id) {
        Optional<Rol> rol = rolRepository.findById(id);
        if (!rol.isPresent()) {
            throw new NotFoundException("error");
        }
        if (rol.get().isSilindiMi()) {
            throw new NotFoundException("error");
        }
        if (!rol.get().getAd().equals(dto.getAd())) {
            rol.get().setAd(dto.getAd());
        }
        if (!rol.get().getKod().equals(dto.getKod())) {
            rol.get().setKod(dto.getKod());
        }

        Rol updatedRol = rolEntityRolDtoMapper.dtoToEntity(dto);
        rolRepository.save(updatedRol);
        dto.setId(updatedRol.getId());
        dto.setId(updatedRol.getId());
        return dto;
    }

    @Override
    public boolean deleteById(String id) {
        Optional<Rol> rol = rolRepository.findById(id);
        if (!rol.isPresent()) {
            throw new NotFoundException("error");
        }
        int listCount = kullaniciRolRepository.findByRolIdIn(rol.get().getId()).get().size();
        if (listCount > 0) {
            throw new NotDeletedException("error");
        }
        rol.get().setSilindiMi(true);
        rolRepository.save(rol.get());
        return true;
    }

    @Override
    public RolDto findById(String id) {
        Optional<Rol> rol = rolRepository.findById(id);
        if (!rol.isPresent() || rol.get().isSilindiMi()) {
            throw new NotFoundException("error");
        }
        return rolEntityRolDtoMapper.entityToDto(rol.get());
    }

    @Override
    public List<RolDto> getUserRolesById(String id) {
        List<RolDto> rolDtoList = new ArrayList<>();
        List<String> rolIds = kullaniciRolService.getRoleIdsByUserId(id);
        if (rolIds.size() > 0) {
            Iterable<Rol> userRol = rolRepository.findAllById(rolIds);
            userRol.forEach(rol -> {
                if (!rol.isSilindiMi()) {
                    rolDtoList.add(rolEntityRolDtoMapper.entityToDto(rol));
                }
            });
        }
        return rolDtoList;
    }
}
