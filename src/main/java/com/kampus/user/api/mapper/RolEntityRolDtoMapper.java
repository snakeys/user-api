package com.kampus.user.api.mapper;

import com.kampus.user.api.dto.RolDto;
import com.kampus.user.api.entity.Rol;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RolEntityRolDtoMapper {

    Rol dtoToEntity(RolDto rolDto);
    RolDto entityToDto(Rol rol);
}
