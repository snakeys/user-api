package com.kampus.user.api.core.exception;

import com.kampus.user.api.core.enums.EnmCode;

public class BaseCustomException extends RuntimeException {
    private EnmCode code;
    private String errorMessage;

    public BaseCustomException(EnmCode code) {
        this.code = code;
    }

    public BaseCustomException(EnmCode code, String errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public EnmCode getCode() {
        return code;
    }

    public void setCode(EnmCode code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
