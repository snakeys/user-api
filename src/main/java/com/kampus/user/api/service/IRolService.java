package com.kampus.user.api.service;

import com.kampus.user.api.core.dto.Dto;
import com.kampus.user.api.core.service.IBaseService;
import com.kampus.user.api.dto.KullaniciDto;
import com.kampus.user.api.dto.RolDto;

import java.util.List;

public interface IRolService<E extends Dto, I extends String> extends IBaseService<RolDto, String> {

    List<RolDto> getUserRolesById(String id);
}
