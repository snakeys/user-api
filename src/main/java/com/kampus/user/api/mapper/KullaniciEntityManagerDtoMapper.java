package com.kampus.user.api.mapper;

import com.kampus.user.api.dto.ManagerDto;
import com.kampus.user.api.entity.Kullanici;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KullaniciEntityManagerDtoMapper {

    List<ManagerDto> entitiesToDtos(List<Kullanici> kullanicis);

}
