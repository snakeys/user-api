package com.kampus.user.api.dto;

import com.kampus.user.api.core.dto.RequestDto;
import lombok.*;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ManagerDto extends RequestDto {
    private String ad;
    private String soyad;
    private String cep;
    private String adres;
    private Long tc;
    private String email;
    private LocalDate dogumTarihi;
    private Integer cinsiyetId;
    private String bransId;
    private boolean silindiMi = false;
}
