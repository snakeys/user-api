package com.kampus.user.api.service.impl;

import com.kampus.user.api.core.dto.RequestDto;
import com.kampus.user.api.core.enums.EnmRol;
import com.kampus.user.api.core.exception.ExistException;
import com.kampus.user.api.core.exception.NotDeletedException;
import com.kampus.user.api.core.exception.NotFoundException;
import com.kampus.user.api.core.helper.StringHelper;
import com.kampus.user.api.core.service.BaseService;
import com.kampus.user.api.core.util.TPage;
import com.kampus.user.api.dto.KullaniciDto;
import com.kampus.user.api.dto.ManagerDto;
import com.kampus.user.api.dto.OgretmenDto;
import com.kampus.user.api.dto.RolDto;
import com.kampus.user.api.dto.api.ApiResponseDto;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.entity.Kullanici;
import com.kampus.user.api.entity.KullaniciRol;
import com.kampus.user.api.entity.Rol;
import com.kampus.user.api.mapper.KullaniciEntityKullaniciDtoMapper;
import com.kampus.user.api.mapper.KullaniciEntityManagerDtoMapper;
import com.kampus.user.api.mapper.KullaniciEntityOgretmenDtoMapper;
import com.kampus.user.api.repository.KullaniciRepository;
import com.kampus.user.api.repository.KullaniciRolRepository;
import com.kampus.user.api.repository.RolRepository;
import com.kampus.user.api.service.IKullaniciService;
import com.kampus.user.api.service.IRolService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.kampus.user.api.core.constants.ApiUrlConstants.M_BRANS_API_URL;
import static com.kampus.user.api.core.constants.ApiUrlConstants.M_OKUL_API_URL;

@Service
@RequiredArgsConstructor
public class KullaniciServiceImpl extends BaseService implements IKullaniciService<RequestDto, String> {

    @Autowired
    KullaniciRepository kullaniciRepository;
    @Autowired
    RolRepository rolRepository;
    @Autowired
    KullaniciEntityKullaniciDtoMapper kullaniciEntityKullaniciDtoMapper;
    @Autowired
    KullaniciEntityManagerDtoMapper kullaniciEntityManagerDtoMapper;
    @Autowired
    KullaniciEntityOgretmenDtoMapper kullaniciEntityOgretmenDtoMapper;
    @Autowired
    KullaniciRolRepository kullaniciRolRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    IRolService rolService;

    @Override
    public KullaniciDto save(KullaniciDto kullaniciDto) {
        Optional<Kullanici> kullanici = kullaniciRepository.findByTc(kullaniciDto.getTc());
        if (kullanici.isPresent()) {
            throw new ExistException("error");
        }
        if (!StringHelper.isNullOrEmpty(kullaniciDto.getBransId())) {
            ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(M_BRANS_API_URL + kullaniciDto.getBransId(), ApiResponseDto.class);
            if (responseDto.getBody() == null && responseDto.getStatusCode() != HttpStatus.OK) {
                throw new NotFoundException("error");
            }
        }
        if (!StringHelper.isNullOrEmpty(kullaniciDto.getOkulId())) {
            ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(M_OKUL_API_URL + kullaniciDto.getOkulId(), ApiResponseDto.class);
            if (responseDto.getBody() == null && responseDto.getStatusCode() != HttpStatus.OK) {
                throw new NotFoundException("error");
            }
        }
        //kps
        String password = firstPassword(kullaniciDto);
        kullaniciDto.setAd(StringHelper.toUpper(kullaniciDto.getAd()));
        kullaniciDto.setSoyad(StringHelper.toUpper(kullaniciDto.getSoyad()));
        kullaniciDto.setCep(kullaniciDto.getCep().replaceAll("\\s+", ""));
        Kullanici newKullanici = kullaniciEntityKullaniciDtoMapper.dtoToEntity(kullaniciDto);
        newKullanici.setSifre(bCryptPasswordEncoder.encode(password));
        HashSet<Rol> rolList;
        rolList = getRoles(kullaniciDto.getRolKods());
        newKullanici.setRoller(rolList);
        kullaniciRepository.save(newKullanici);

        setKullaniciRol(rolList, newKullanici);
        kullaniciDto.setId(newKullanici.getId());
        return kullaniciDto;

    }

    @Override
    public KullaniciDto updateById(KullaniciDto kullaniciDto, String id) {
        Optional<Kullanici> kullanici = kullaniciRepository.findById(id);
        if (!kullanici.isPresent()) {
            throw new NotFoundException("error");
        }
        if (kullanici.get().isSilindiMi()) {
            throw new NotFoundException("error");
        }

        if (kullanici.get().getTc() != kullaniciDto.getTc()) {
            if (existTcNumber(kullaniciDto.getTc())) {
                throw new ExistException("error");
            }
        }
        if (!StringHelper.isNullOrEmpty(kullaniciDto.getBransId())) {
            ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(M_BRANS_API_URL + kullaniciDto.getBransId(), ApiResponseDto.class);
            if (responseDto.getBody() == null && responseDto.getStatusCode() != HttpStatus.OK) {
                throw new NotFoundException("error");
            }
        }
        if (!StringHelper.isNullOrEmpty(kullaniciDto.getOkulId())) {
            ResponseEntity<ApiResponseDto> responseDto = restTemplate.getForEntity(M_OKUL_API_URL + kullaniciDto.getOkulId(), ApiResponseDto.class);
            if (responseDto.getBody() == null && responseDto.getStatusCode() != HttpStatus.OK) {
                throw new NotFoundException("error");
            }
        }

        kullaniciDto.setAd(StringHelper.toUpper(kullaniciDto.getAd()));
        kullaniciDto.setSoyad(StringHelper.toUpper(kullaniciDto.getSoyad()));
        kullaniciDto.setCep(kullaniciDto.getCep().replaceAll("\\s+", ""));

        Kullanici newKullanici = kullaniciEntityKullaniciDtoMapper.dtoToEntity(kullaniciDto);
        HashSet<Rol> rolList;
        rolList = getRoles(kullaniciDto.getRolKods());
        newKullanici.setRoller(rolList);
        kullaniciRepository.save(newKullanici);
        kullaniciDto.setId(newKullanici.getId());
        setKullaniciRol(rolList, newKullanici);
        return kullaniciDto;
    }

    @Override
    public boolean deleteById(String kullaniciId) {
        Optional<Kullanici> kullanici = kullaniciRepository.findById(kullaniciId);
        if (!kullanici.isPresent()) {
            throw new NotDeletedException("error");
        }
        kullanici.get().setSilindiMi(true);
        kullaniciRepository.save(kullanici.get());
        return true;
    }

    @Override
    public KullaniciDto findById(String id) {
        Optional<Kullanici> kullanici = kullaniciRepository.findById(id);
        if (!kullanici.isPresent()) {
            throw new NotFoundException("error");
        }
        if (kullanici.get().isSilindiMi()) {
            throw new NotFoundException("error");
        }
        KullaniciDto kullaniciDto = kullaniciEntityKullaniciDtoMapper.entityToDto(kullanici.get());
        List<RolDto> userRolList = rolService.getUserRolesById(id);
        List<String> rolKods = new ArrayList<>();
        userRolList.forEach(rol -> {
            rolKods.add(rol.getKod());
        });
        kullaniciDto.setRolKods(rolKods);
        return kullaniciDto;
    }

    @Override
    public Kullanici getKullaniciByUsername(String username) {
        Optional<Kullanici> kullanici = kullaniciRepository.findByEmail(username);
        if (!kullanici.isPresent()) {
            try {
                long tc = Long.parseLong(username);
                kullanici = kullaniciRepository.findByTc(tc);

            } catch (NumberFormatException e) {
            }

            if (!kullanici.isPresent()) {
                throw new NotFoundException("error");
            }
        }
        if (!kullanici.get().isSilindiMi()) {
            return kullanici.get();
        } else {
            throw new NotFoundException("error");
        }

    }

    @Override
    public TPage<ManagerDto> findByPagingCriteriaManager(KullaniciFilterDto kullaniciFilterDto, Pageable pageable) {
        Optional<Rol> rol = rolRepository.findByKod(EnmRol.OKUL_YONETICISI.getKod());
        List<Kullanici> kullaniciList = kullaniciRepository.findKullanicisByProperties(kullaniciFilterDto, pageable);
        List<Kullanici> managerList = new ArrayList<>();
        kullaniciList.forEach(kullanici -> {
            kullanici.getRoller().forEach(
                    userRol -> {
                        if (userRol.getKod().equals(rol.get().getKod())) {
                            managerList.add(kullanici);
                            return;
                        }
                    }
            );
        });
        List<ManagerDto> data = kullaniciEntityManagerDtoMapper.entitiesToDtos(managerList);
        TPage<ManagerDto> response = new TPage<>();
        response.setResult(data);

        return response;
    }

    @Override
    public TPage<OgretmenDto> findByPagingCriteriaOgretmen(KullaniciFilterDto kullaniciFilterDto, Pageable pageable) {
        Optional<Rol> rol = rolRepository.findByKod(EnmRol.OGRETMEN.getKod());
        List<Kullanici> kullaniciList = kullaniciRepository.findKullanicisByProperties(kullaniciFilterDto, pageable);
        List<Kullanici> managerList = new ArrayList<>();
        kullaniciList.forEach(kullanici -> {
            kullanici.getRoller().forEach(
                    userRol -> {
                        if (userRol.getKod().equals(rol.get().getKod())) {
                            managerList.add(kullanici);
                            return;
                        }
                    }
            );
        });
        List<OgretmenDto> data = kullaniciEntityOgretmenDtoMapper.entitiesToDtos(managerList);
        TPage<OgretmenDto> response = new TPage<>();
        response.setResult(data);

        return response;
    }

    private HashSet<Rol> getRoles(List<String> rolKods) {
        HashSet<Rol> rols = new HashSet<>();
        if (!rolKods.isEmpty()) {
            rolKods.forEach(kod -> {
                Optional<Rol> rol = rolRepository.findByKod(kod);
                if (rol.isPresent() && !rol.get().isSilindiMi()) {
                    rols.add(rol.get());
                }
            });
            return rols;
        }
        return null;
    }

    private boolean existTcNumber(Long tc) {
        return kullaniciRepository.findByTc(tc).isPresent();
    }

    private String firstPassword(KullaniciDto kullaniciDto) {

        if (kullaniciDto.getTc() != null) {
            return kullaniciDto.getTc().toString().substring(5, 11);
        }

        return "123456";
    }

    private void setKullaniciRol(Set<Rol> rolList, Kullanici newKullanici) {
        for (Rol rol : rolList) {
            KullaniciRol kullaniciRol = new KullaniciRol();
            kullaniciRol.setKullaniciId(newKullanici.getId());
            kullaniciRol.setRolId(rol.getId());
            kullaniciRolRepository.save(kullaniciRol);
        }
    }


}
