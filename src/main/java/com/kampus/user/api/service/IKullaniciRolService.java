package com.kampus.user.api.service;

import com.kampus.user.api.core.dto.Dto;
import com.kampus.user.api.core.service.IBaseService;
import com.kampus.user.api.dto.KullaniciRolDto;

import java.util.List;

public interface IKullaniciRolService<E extends Dto, I extends String> extends IBaseService<KullaniciRolDto, String> {

    List<String> getRoleIdsByUserId(String userId);
}
