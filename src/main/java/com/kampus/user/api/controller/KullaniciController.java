package com.kampus.user.api.controller;

import com.kampus.user.api.core.controller.GenericRestController;
import com.kampus.user.api.core.dto.ResponseDto;
import com.kampus.user.api.core.util.TPage;
import com.kampus.user.api.dto.KullaniciDto;
import com.kampus.user.api.dto.OgrenciDto;
import com.kampus.user.api.dto.filter.KullaniciFilterDto;
import com.kampus.user.api.entity.Kullanici;
import com.kampus.user.api.service.IKullaniciService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/kullanici")
@AllArgsConstructor
public class KullaniciController extends GenericRestController<KullaniciDto, String, IKullaniciService<KullaniciDto, String>> {

    @Autowired
    IKullaniciService kullaniciService;

    @Override
    public ResponseDto create(@RequestBody KullaniciDto dto) {
        final KullaniciDto savedDto = (KullaniciDto) kullaniciService.save(dto);
        return ResponseDto.done(getMessageFromSource("success"), savedDto);
    }

    @Override
    public ResponseDto update(@PathVariable(value = "id") String id, @RequestBody KullaniciDto dto) {
        final KullaniciDto updatedDto = (KullaniciDto) kullaniciService.updateById(dto, id);
        return ResponseDto.done(getMessageFromSource("success"), updatedDto);
    }

    @Override
    public ResponseDto getById(@PathVariable(value = "id") String id) {
        final KullaniciDto kullaniciDto = (KullaniciDto) kullaniciService.findById(id);
        return ResponseDto.done(getMessageFromSource("success"), kullaniciDto);
    }

    @GetMapping("/auth/{username}")
    public Kullanici getUserByUserName(@PathVariable(value = "username") String username) {
        Kullanici kullanici = kullaniciService.getKullaniciByUsername(username);
        return kullanici;
    }

    @GetMapping("/pagination-filter-manager")
    public ResponseDto getAllManagerByPagination(Pageable pageable,
                                                 KullaniciFilterDto kullaniciFilterDto) {

        TPage<OgrenciDto> data = kullaniciService.findByPagingCriteriaManager(
                kullaniciFilterDto,
                pageable);

        return ResponseDto.done(getMessageFromSource("success"), data);
    }

    @GetMapping("/pagination-filter-ogretmen")
    public ResponseDto getAllOgretmenByPagination(Pageable pageable,
                                                  KullaniciFilterDto kullaniciFilterDto) {

        TPage<OgrenciDto> data = kullaniciService.findByPagingCriteriaOgretmen(
                kullaniciFilterDto,
                pageable);

        return ResponseDto.done(getMessageFromSource("success"), data);
    }


}
